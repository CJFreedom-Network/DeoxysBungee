package uk.co.atlasmedia;

import net.md_5.bungee.api.plugin.Plugin;
import uk.co.atlasmedia.listeners.ListenerRegistrar;
import uk.co.atlasmedia.networking.*;

import java.sql.SQLException;

public class DeoxysBungee extends Plugin
{
    public static DeoxysBungee plugin;

    public SQLManager sqlManager;
    public RedisManager redisManager;

    //TODO Move usernames/passwords to configs in case src code is leaked.

    @Override
    public void onEnable()
    {
        plugin = this;
        sqlManager = new SQLManager("localhost", "3306", "deoxyscore", "root", "SVcP94IhvMk1hgo29lW");
        //sqlManager = new SQLManager("151.80.69.197", "3306", "deoxyscore", "root", "test");
        redisManager = new RedisManager("192.168.1.22", 6379, null);
        //redisManager = new RedisManager("151.80.69.197", 6379, "P@ssword12345");
        //redisManager = new RedisManager("redis-10079.c14.us-east-1-2.ec2.cloud.redislabs.com", 10079, null);
        getProxy().getPluginManager().registerListener(this, new ListenerRegistrar());
    }
}
