package uk.co.atlasmedia.networking;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
//import redis.clients.jedis.Jedis;
import uk.co.atlasmedia.DeoxysBungee;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Synchronizer
{
    public void onPostLogin(PostLoginEvent event)
    {
        DeoxysBungee.plugin.getLogger().info("Syncing player to Redis.");
        String uuid = event.getPlayer().getUniqueId().toString();

        try
        {
            if (DeoxysBungee.plugin.redisManager.getValue(uuid, 3) != null)
            {
                event.getPlayer().disconnect(new TextComponent("You must wait a few seconds before joining again."));
                return;
            }

            //TODO Online Players/Current server
            DeoxysBungee.plugin.redisManager.setValue(uuid, uuid, 3); //This gets set first, because we don't want to have player data overwritten when someone relogs.

            //Rank
            String rank = (String) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "rank", "players");
            DeoxysBungee.plugin.redisManager.setValue(uuid, rank, 0);

            //Coins
            Integer coins = (Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players");
            DeoxysBungee.plugin.redisManager.setValue(uuid, coins.toString(), 1);

            //Mutes //TODO Mute expiry will be handled when a player talks.
            if (DeoxysBungee.plugin.sqlManager.isMutedSQL(uuid))
            {
                String expiry = DeoxysBungee.plugin.sqlManager.getExpiryTimeSQL(uuid);
                DeoxysBungee.plugin.redisManager.setValue(uuid, expiry, 2);
            }

            //TODO Add personal mutes.
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        /*DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));
        DeoxysBungee.plugin.getLogger().info((String) DeoxysBungee.plugin.redisManager.getValue(uuid, 1));

        try
        {
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
            DeoxysBungee.plugin.getLogger().info(((Integer) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "coins", "players")).toString());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }*/
    }

    public void onPlayerDisconnect(PlayerDisconnectEvent event)
    {
        //We must check that everything is not null before syncing, because this event will be run even when someone is banned.
        DeoxysBungee.plugin.getLogger().info("Syncing player to MySQL.");
        String uuid = event.getPlayer().getUniqueId().toString();

        try
        {
            if (DeoxysBungee.plugin.redisManager.getValue(uuid, 3) == null)
            {
                return;
            }

            //Rank
            String rank = (String) DeoxysBungee.plugin.redisManager.getValue(uuid, 0);
            if (rank != null) // Sometimes players join and leave immediately after.
            {
                DeoxysBungee.plugin.sqlManager.updateInTable("uuid", uuid, rank, "rank", "players");
                DeoxysBungee.plugin.redisManager.deleteObject(uuid, 0);
            }

            //Coins
            Integer coins = new Integer(DeoxysBungee.plugin.redisManager.getValue(uuid, 1).toString());
            if (coins != null)
            {
                DeoxysBungee.plugin.sqlManager.updateInTable("uuid", uuid, coins, "coins", "players");
                DeoxysBungee.plugin.redisManager.deleteObject(uuid, 1);
            }

            //Mutes We only push to SQL if the player wasn't already muted. An unmute command will drop a user's entry from SQL AND Redis.
            if (DeoxysBungee.plugin.redisManager.isMutedRedis(uuid))
            {
                if (!DeoxysBungee.plugin.sqlManager.isMutedSQL(uuid))
                {
                    String expires = DeoxysBungee.plugin.redisManager.getMuteExpiryTimeRedis(uuid);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
                    Date parsedDate = dateFormat.parse(expires);
                    Timestamp timestamp = new Timestamp(parsedDate.getTime());
                    DeoxysBungee.plugin.sqlManager.setMuted(uuid, timestamp);
                }
                DeoxysBungee.plugin.redisManager.deleteObject(uuid, 2);
            }

            //Online players/current server
            DeoxysBungee.plugin.redisManager.deleteObject(uuid, 3);
        }
        catch (SQLException | ParseException e)
        {
            e.printStackTrace();
        }
    }
}
