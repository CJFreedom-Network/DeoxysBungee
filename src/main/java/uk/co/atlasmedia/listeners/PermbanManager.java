package uk.co.atlasmedia.listeners;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import uk.co.atlasmedia.DeoxysBungee;

import java.sql.SQLException;

public class PermbanManager
{
    public void onPostLogin(PostLoginEvent event)
    {
        ProxiedPlayer player = event.getPlayer();
        String uuid = event.getPlayer().getUniqueId().toString();
        String ip = event.getPlayer().getAddress().getAddress().getHostAddress();

        boolean banned = true;
        boolean ipBanned = true;
        String reason = null;
        String bannedBy = null;

        try
        {
            banned = isUUIDBanned(uuid);
            ipBanned = isIPBanned(ip);

            if (banned)
            {
                bannedBy = getUUIDBanner(uuid);
            }
            if (getUUIDBanReason(uuid) != null && !getUUIDBanReason(uuid).equals(""))
            {
                reason = getUUIDBanReason(uuid);
            }
        }
        catch (SQLException e)
        {
            DeoxysBungee.plugin.getLogger().severe("Error querying SQL database for bans.");
            e.printStackTrace();
        }

        if (banned && reason != null)
        {
            player.disconnect(new TextComponent("You have been banned for: " + reason + "\nBanned by: " + bannedBy));
            return;
        }

        if (banned) //Reason is null.
        {
            player.disconnect(new TextComponent("You have been banned.\nBanned by: " + bannedBy));
            return;
        }

        if (ipBanned) //Not uuid banned, but ip banned.
        {
            player.disconnect(new TextComponent("You have been banned."));
        }
    }

    public boolean isUUIDBanned(String uuid) throws SQLException
    {
        return DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "uuid", "banned_players") != null;
    }

    private String getUUIDBanReason(String uuid) throws SQLException
    {
        return (String) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "reason", "banned_players");
    }

    private String getUUIDBanner(String uuid) throws SQLException
    {
        return (String) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "banned_by", "banned_players");
    }

    public boolean isIPBanned(String ip) throws SQLException
    {
        return DeoxysBungee.plugin.sqlManager.getFromTable("ip", ip, "ip", "banned_ips") != null;
    }
}
