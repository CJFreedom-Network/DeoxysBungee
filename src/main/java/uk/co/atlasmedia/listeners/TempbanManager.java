package uk.co.atlasmedia.listeners;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.event.EventHandler;
import uk.co.atlasmedia.DeoxysBungee;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TempbanManager
{
    /*
        When tempbans are added, the time will be converted to GMT, and 'x' hours/minutes will be added. Then they are stored in the database.
        When tempbans are validated, the time will be converted to GMT and checked against the database.
        Everthing is stored as strings, because I'm too lazy to figure out the Timestamp class.
    */

    @EventHandler
    public void onPostLogin(PostLoginEvent event)
    {
        //TODO GMT based bans.

        ProxiedPlayer player = event.getPlayer();
        String uuid = event.getPlayer().getUniqueId().toString();

        try
        {
            if (!isTempbanned(uuid))
            {
                return;
            }

            if (banIsExpired(getExpiryTime(uuid)))
            {
                removeTempban(uuid);
                return;
            }
            else
            {   //TODO More professional messages.
                player.disconnect(new TextComponent("You have been tempbanned.\nReason: " + getReason(uuid) + "\nBanned by: " + getBanner(uuid) + "\n Expries on: " + getExpiryTime(uuid) + " GMT."));
            }
        }
        catch (SQLException | ParseException e)
        {
            e.printStackTrace();
        }
    }

    public boolean isTempbanned(String uuid) throws SQLException
    {
        return DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "uuid", "tempbans") != null;
    }

    private String getBanner(String uuid) throws SQLException
    {
        return (String) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "banned_by", "tempbans");
    }

    private String getReason(String uuid) throws SQLException
    {
        return (String) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "reason", "tempbans");
    }

    //TODO Broken
    private boolean banIsExpired(Long expiry_time) throws ParseException
    {
        long now = new Date().getTime();
        long expires = expiry_time;
        /*Date today = new Date();
        Timestamp now = new Timestamp(today.getTime());
        return now.after(expiry_time);*/
        return (now - expires) >= 0 ;
    }

    //TODO Turn into getTimestamp() in SQLManager.
    private long getExpiryTime(String uuid) throws SQLException
    {
        /*Connection c = DeoxysBungee.plugin.sqlManager.getConnection();
        PreparedStatement statement = c.prepareStatement("SELECT * FROM `tempbans` WHERE `uuid` = ?");
        statement.setString(1, uuid);
        ResultSet res = statement.executeQuery();
        if(res.next())
        {
            return res.getTimestamp("expires_on");
        }
        //DeoxysBungee.plugin.sqlManager.closeConnection();
        return null;*/

        return (Long) DeoxysBungee.plugin.sqlManager.getFromTable("uuid", uuid, "expires_on", "tempbans");
    }

    private void removeTempban(String uuid) throws SQLException
    {
        DeoxysBungee.plugin.sqlManager.deleteFromTable("uuid", uuid, "tempbans");
    }
}
