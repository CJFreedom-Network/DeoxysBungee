package uk.co.atlasmedia.listeners;

import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import uk.co.atlasmedia.networking.Synchronizer;

import java.sql.SQLException;

public class ListenerRegistrar implements Listener
{
    private PermbanManager pbM = new PermbanManager();
    private TempbanManager tbM = new TempbanManager();
    private Synchronizer sync = new Synchronizer();

    //TODO Mop this up. There's probably a cleaner and more efficient way to do this.

    //TODO These don't need the onPostLogin annotation.

    @EventHandler
    public void onPostLogin(PostLoginEvent event)
    {
        String uuid = event.getPlayer().getUniqueId().toString();

        pbM.onPostLogin(event);
        tbM.onPostLogin(event);

        try
        {
            //We don't bother syncing to Redis if the player is banned.
            if (!pbM.isUUIDBanned(uuid) && !pbM.isIPBanned(event.getPlayer().getAddress().getAddress().getHostAddress()) && !tbM.isTempbanned(uuid))
            {
                sync.onPostLogin(event);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent event)
    {
        //TODO If a player is tempbanned while online, the next time they log in, they will get a "You must not join this quickly" message.
        //Syncing disconnects even when a player is banned will solve this problem, because it will remove their online entry from redis.

        sync.onPlayerDisconnect(event);
    }
}
