package uk.co.atlasmedia.listeners;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.PlayerHandshakeEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.mindrot.jbcrypt.BCrypt;
import uk.co.atlasmedia.DeoxysBungee;

import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SubdomainListener implements Listener
{
    //This is an old class. It is only here because it might be useful in the future.
    private HashMap<InetSocketAddress, String> map = new HashMap<InetSocketAddress, String>();

    @EventHandler
    public void onPlayerHandshake(PlayerHandshakeEvent event)
    {
        map.put(event.getConnection().getAddress(), event.getHandshake().getHost().split("\\.")[0]);
    }

    @EventHandler
    public void onPlayerLogin(PostLoginEvent event)
    {
        String name = event.getPlayer().getName();
        InetSocketAddress address = event.getPlayer().getAddress();
        String subdomain = map.get(address);

        try
        {
            if (!hasSecurelyLoggedIn(event.getPlayer().getName())) //TODO Move this to the inital join event.
            {
                setSubdomain(event.getPlayer().getName(), "test"); //TODO Assign subdomain randomly
                event.getPlayer().disconnect(new TextComponent("Welcome to our server... join with: bla.bla.bla.")); //TODO Change
                setSecurelyLoggedIn(name, 1); //TODO See if you can change the int to a boolean instead
            }
            if (!subdomainIsValid(name, subdomain) && hasSecurelyLoggedIn(event.getPlayer().getName()))
            {
                event.getPlayer().disconnect(new TextComponent("Your subdomain is incorrect.")); //TODO Change this to a more professional message.
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean subdomainIsValid(String name, String subdomain) throws SQLException
    {
        String hashedSubdomain = (String) DeoxysBungee.plugin.sqlManager.getFromTable("name", name, "subdomain", "players");
        return BCrypt.checkpw(subdomain, hashedSubdomain);
    }

    public void setSubdomain(String name, String subdomain) throws SQLException
    {
        String hashedSubdomain = BCrypt.hashpw(subdomain, BCrypt.gensalt(5));
        DeoxysBungee.plugin.sqlManager.updateInTable("name", name, hashedSubdomain, "subdomain", "players");
    }

    public boolean hasSecurelyLoggedIn(String name) throws SQLException
    {
        return (Integer) DeoxysBungee.plugin.sqlManager.getFromTable("name", name, "has_securely_logged_in", "players") != 0;
    }

    public void setSecurelyLoggedIn(String name, int bool) throws SQLException
    {
        DeoxysBungee.plugin.sqlManager.updateInTable("name", name, bool, "has_securely_logged_in", "players");
    }
}
